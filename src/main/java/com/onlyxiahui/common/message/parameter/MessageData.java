package com.onlyxiahui.common.message.parameter;

/**
 * 接受请求信息<br>
 * Date 2018-12-25 10:01:07<br>
 * 
 * @author XiaHui<br>
 * @param <H> 头部
 * @param <B> 主体
 * @since 1.0.0
 */

public class MessageData<H, B> {

	/**
	 * 头部
	 */
	private H head;
	/**
	 * 主体
	 */
	private B body;

	public H getHead() {
		return head;
	}

	public void setHead(H head) {
		this.head = head;
	}

	public B getBody() {
		return body;
	}

	public void setBody(B body) {
		this.body = body;
	}
}
