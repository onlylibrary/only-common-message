package com.onlyxiahui.common.message.node;

/**
 * 
 * 信息头部<br>
 * Date 2018-12-25 09:29:03<br>
 * 
 * @author XiaHui<br>
 * @since 1.0.0
 */
public interface Head {

	/**
	 * 设置信息的唯一值<br>
	 * Date 2019-01-06 10:06:22<br>
	 * 
	 * @param key
	 * @since 1.0.0
	 */
	public void setKey(String key);

	/**
	 * 获取信息唯一值<br>
	 * Date 2019-01-06 10:06:45<br>
	 * 
	 * @return String
	 * @since 1.0.0
	 */
	public String getKey();
}
