package com.onlyxiahui.common.message.result;

import java.util.HashMap;

/**
 * 信息<br>
 * Date 2020-01-14 20:44:04<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @param <B> 信息主体
 * @since 1.0.0
 */
public class ResultBodyMessage<B> extends AbstractResultMessage<B> {

	@SuppressWarnings("unchecked")
	public ResultBodyMessage() {
		setBody((B) new HashMap<>(0));
	}

	public ResultBodyMessage(B body) {
		setBody(body);
	}
}
