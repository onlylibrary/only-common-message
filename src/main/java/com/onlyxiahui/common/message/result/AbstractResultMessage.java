package com.onlyxiahui.common.message.result;

import com.onlyxiahui.common.message.AbstractMessage;
import com.onlyxiahui.common.message.bean.Info;

/**
 * 信息<br>
 * Date 2018-12-25 10:58:04<br>
 * 
 * @author XiaHui<br>
 * @param <B> 主体
 * @since 1.0.0
 */
public abstract class AbstractResultMessage<B> extends AbstractMessage<B> {

	/**
	 * 消息状态信息（成功/失败）
	 */
	private Info info = new Info();

	public AbstractResultMessage() {
		super();
	}

	public void setInfo(Info info) {
		this.info = info;
	}

	public Info getInfo() {
		return info;
	}

	public void setSuccess(boolean success) {
		info.setSuccess(success);
	}

	public void addError(String code, String text) {
		info.addError(code, text);
	}

	public void addWarning(String code, String text) {
		info.addWarning(code, text);
	}

	public void addPrompt(String code, String text) {
		info.addPrompt(code, text);
	}
}
