package com.onlyxiahui.common.message.result;

import java.util.HashMap;
import java.util.Map;

/**
 * 信息<br>
 * Date 2018-12-25 10:58:27<br>
 * 
 * @author XiaHui<br>
 * @since 1.0.0
 */
public class ResultMessage extends AbstractResultMessage<Map<String, Object>> {

	/**
	 * 信息主体
	 */
	private Map<String, Object> body = new HashMap<>();

	public ResultMessage() {
		super();
		this.setBody(body);
	}

	public ResultMessage(Map<String, Object> body) {
		this.setBody(body);
	}

	public ResultMessage(String key, Object data) {
		body.put(key, data);
	}

	public Object bodyPut(String key, Object value) {
		return body.put(key, value);
	}

	public Object bodyGet(String key) {
		return body.get(key);
	}
}
