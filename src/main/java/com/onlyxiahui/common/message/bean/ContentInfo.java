package com.onlyxiahui.common.message.bean;

/**
 * 错误/警告/提示等内容 <br>
 * Date 2018-12-25 09:25:34<br>
 * 
 * @author XiaHui<br>
 * @since 1.0.0
 */
public class ContentInfo {

	/**
	 * 信息编码
	 */
	private String code;
	/**
	 * 信息内容
	 */
	private String text;

	public ContentInfo() {

	}

	public ContentInfo(String code, String text) {
		this.code = code;
		this.text = text;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
}
