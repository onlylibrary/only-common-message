package com.onlyxiahui.common.message.bean;

import java.util.ArrayList;
import java.util.List;

/**
 * 信息结果状态，内含成功提示、错误码、警告码<br>
 * Date 2018-12-25 09:31:59<br>
 *
 * @author XiaHui<br>
 * @since 1.0.0
 */
public class Info {

	/**
	 * 信息状态：成功：true、失败：false
	 */
	private boolean success = true;
	/**
	 * 程序错误信息集合（主要是程序级别错误，如：字段错误、不能为空等）
	 */
	private List<ContentInfo> errors = new ArrayList<>();
	/**
	 * 业务错误信息集合（主要是业务中错误，如：账号错误、密码错误等）
	 */
	private List<ContentInfo> warnings = new ArrayList<>();
	/**
	 * 成功提醒
	 */
	private List<ContentInfo> prompts = new ArrayList<>();

	public boolean isSuccess() {
		return success && isEmpty(errors) && isEmpty(warnings);
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public List<ContentInfo> getErrors() {
		return errors;
	}

	public void setErrors(List<ContentInfo> errors) {
		this.errors = errors;
	}

	public List<ContentInfo> getWarnings() {
		return warnings;
	}

	public void setWarnings(List<ContentInfo> warnings) {
		this.warnings = warnings;
	}

	public List<ContentInfo> getPrompts() {
		return prompts;
	}

	public void setPrompts(List<ContentInfo> prompts) {
		this.prompts = prompts;
	}

	/**
	 * 添加错误信息<br>
	 * Date 2019-01-06 10:39:59<br>
	 * 
	 * @param code
	 * @param text
	 * @since 1.0.0
	 */
	public void addError(String code, String text) {
		get(errors).add(new ContentInfo(code, text));
		setSuccess(isEmpty(errors) && isEmpty(warnings));
	}

	public void addWarning(String code, String text) {
		get(warnings).add(new ContentInfo(code, text));
		setSuccess(isEmpty(errors) && isEmpty(warnings));
	}

	public void addPrompt(String code, String text) {
		get(prompts).add(new ContentInfo(code, text));
	}

	private List<ContentInfo> get(List<ContentInfo> list) {
		if (list == null) {
			list = new ArrayList<ContentInfo>();
		}
		return list;
	}

	private boolean isEmpty(List<?> list) {
		return null == list || list.isEmpty();
	}
}
