package com.onlyxiahui.common.message.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.onlyxiahui.common.message.bean.ContentInfo;
import com.onlyxiahui.common.message.bean.Info;

/**
 * 
 * Date 2018-12-25 10:58:58<br>
 * 
 * @author XiaHui<br>
 * @since 1.0.0
 */
public class OnlyMessageUtil {

	/**
	 * 
	 * Date 2018-12-25 11:04:34<br>
	 * Description 获取默认的错误提示
	 * 
	 * @param info
	 * @return String
	 * @since 1.0.0
	 */
	public static String getDefaultWarningText(Info info) {
		String error = "";
		if (info.getWarnings().size() > 0) {
			error = info.getWarnings().get(0).getText();
		} else if (info.getErrors().size() > 0) {
			error = info.getErrors().get(0).getText();
		}
		return error;
	}

	/**
	 * 
	 * Date 2018-12-25 11:02:42<br>
	 * Description 获取默认的错误信息
	 * 
	 * @param info
	 * @return String
	 * @since 1.0.0
	 */
	public static String getDefaultErrorText(Info info) {
		String error = "";
		if (info.getErrors().size() > 0) {
			error = info.getErrors().get(0).getText();
		} else if (info.getWarnings().size() > 0) {
			error = info.getWarnings().get(0).getText();
		}
		return error;
	}

	///////////////////////////////////////////////////

	/**
	 * 
	 * Date 2018-12-25 11:02:26<br>
	 * Description 获取第一个错误信息
	 * 
	 * @param info
	 * @return ContentInfo
	 * @since 1.0.0
	 */
	public static ContentInfo getFirstError(Info info) {
		ContentInfo error = null;
		if (!info.isSuccess()) {
			List<ContentInfo> errors = info.getErrors();
			if (null != errors && !errors.isEmpty()) {
				error = errors.get(0);
			}
		}
		return error;
	}

	/**
	 * 
	 * Date 2018-12-25 11:02:12<br>
	 * Description 获取指定的错误信息
	 * 
	 * @param info
	 * @param code
	 * @return ContentInfo
	 * @since 1.0.0
	 */
	public static ContentInfo getErrorInfo(Info info, String code) {
		ContentInfo error = null;
		if (!info.isSuccess()) {
			List<ContentInfo> errors = info.getErrors();
			if (null != errors) {
				for (ContentInfo e : errors) {
					String c = (null == e.getCode()) ? "" : e.getCode().trim().replace(" ", "");
					if (c.equalsIgnoreCase(code)) {
						error = e;
						break;
					}
				}
			}
		}
		return error;
	}

	/**
	 * 
	 * Date 2018-12-25 11:01:58<br>
	 * Description 获取指定的错误信息文本
	 * 
	 * @param info
	 * @param code
	 * @return String
	 * @since 1.0.0
	 */
	public static String getErrorText(Info info, String code) {
		String error = null;
		if (!info.isSuccess()) {
			List<ContentInfo> errors = info.getErrors();
			if (null != errors) {
				for (ContentInfo e : errors) {
					String c = (null == e.getCode()) ? "" : e.getCode().trim().replace(" ", "");
					if (c.equalsIgnoreCase(code)) {
						error = e.getText();
						break;
					}
				}
			}
		}
		return error;
	}

	/**
	 * 
	 * Date 2018-12-25 11:01:44<br>
	 * Description 根据编码获取错误map
	 * 
	 * @param info
	 * @param codes
	 * @return Map<String, String>
	 * @since 1.0.0
	 */
	public static Map<String, String> getErrorMap(Info info, String... codes) {
		int size = (null != info && null != info.getErrors()) ? info.getErrors().size() : 0;
		Map<String, String> errorMap = new HashMap<>(size);
		if (null != info && !info.isSuccess()) {
			List<ContentInfo> errors = info.getErrors();
			if (null != errors) {
				for (String code : codes) {
					for (ContentInfo e : errors) {
						String c = (null == e.getCode()) ? "" : e.getCode().trim().replace(" ", "");
						if (c.equalsIgnoreCase(code)) {
							errorMap.put(code, e.getText());
							break;
						}
					}
				}
			}
		}
		return errorMap;
	}

	/**
	 * 
	 * Date 2018-12-25 11:01:33<br>
	 * Description
	 * 
	 * @param map
	 * @param info
	 * @param text
	 * @return String
	 * @since 1.0.0
	 */
	public static String getErrorText(Map<String, String> map, Info info, String text) {
		String error = null;
		if (!info.isSuccess()) {
			List<ContentInfo> errors = info.getErrors();
			if (null != errors) {
				for (ContentInfo e : errors) {
					String code = (null == e.getCode()) ? "" : e.getCode().trim().replace(" ", "");
					if (map.containsKey(code)) {
						error = map.get(code);
						break;
					}
				}
			} else {
				error = text;
			}
		}
		return error;
	}

	/////////////////////////////////////

	/**
	 * 
	 * Date 2018-12-25 11:01:14<br>
	 * Description 获取第一个错误信息
	 * 
	 * @param info
	 * @return ContentInfo
	 * @since 1.0.0
	 */
	public static ContentInfo getFirstWarning(Info info) {
		ContentInfo warning = null;
		if (!info.isSuccess()) {
			List<ContentInfo> warnings = info.getWarnings();
			if (null != warnings && !warnings.isEmpty()) {
				warning = warnings.get(0);
			}
		}
		return warning;
	}

	/**
	 * 
	 * Date 2018-12-25 11:01:00<br>
	 * Description 获取指定的错误信息
	 * 
	 * @param info
	 * @param code
	 * @return ContentInfo
	 * @since 1.0.0
	 */
	public static ContentInfo getWarningInfo(Info info, String code) {
		ContentInfo warning = null;
		if (!info.isSuccess()) {
			List<ContentInfo> warnings = info.getWarnings();
			if (null != warnings) {
				for (ContentInfo e : warnings) {
					String c = (null == e.getCode()) ? "" : e.getCode().trim().replace(" ", "");
					if (c.equalsIgnoreCase(code)) {
						warning = e;
						break;
					}
				}
			}
		}
		return warning;
	}

	/**
	 * 
	 * Date 2018-12-25 11:00:29<br>
	 * Description 获取指定的错误信息文本
	 * 
	 * @param info
	 * @param code
	 * @return String
	 * @since 1.0.0
	 */
	public static String getWarningText(Info info, String code) {
		String warning = null;
		if (!info.isSuccess()) {
			List<ContentInfo> warnings = info.getWarnings();
			if (null != warnings) {
				for (ContentInfo e : warnings) {
					String c = (null == e.getCode()) ? "" : e.getCode().trim().replace(" ", "");
					if (c.equalsIgnoreCase(code)) {
						warning = e.getText();
						break;
					}
				}
			}
		}
		return warning;
	}

	/**
	 * 
	 * Date 2018-12-25 11:00:09<br>
	 * Description 根据编码获取错误map
	 * 
	 * @param info
	 * @param codes
	 * @return Map<String, String>
	 * @since 1.0.0
	 */
	public static Map<String, String> getWarningMap(Info info, String... codes) {
		int size = (null != info && null != info.getWarnings()) ? info.getWarnings().size() : 0;
		Map<String, String> warningMap = new HashMap<>(size);
		if (null != info && !info.isSuccess()) {
			List<ContentInfo> warnings = info.getWarnings();
			if (null != warnings) {
				for (String code : codes) {
					for (ContentInfo e : warnings) {
						String c = (null == e.getCode()) ? "" : e.getCode().trim().replace(" ", "");
						if (c.equalsIgnoreCase(code)) {
							warningMap.put(code, e.getText());
							break;
						}
					}
				}
			}
		}
		return warningMap;
	}

	/**
	 * 
	 * Date 2018-12-25 10:59:47<br>
	 * Description
	 * 
	 * @param textMap
	 * @param info
	 * @param defaultText
	 * @return String
	 * @since 1.0.0
	 */
	public static String getWarningText(Map<String, String> textMap, Info info, String defaultText) {
		String warning = null;
		if (!info.isSuccess()) {
			List<ContentInfo> warnings = info.getWarnings();
			if (null != warnings) {
				for (ContentInfo e : warnings) {
					String code = (null == e.getCode()) ? "" : e.getCode().trim().replace(" ", "");
					if (textMap.containsKey(code)) {
						warning = textMap.get(code);
						break;
					}
				}
			} else {
				warning = defaultText;
			}
		}
		return warning;
	}
}
