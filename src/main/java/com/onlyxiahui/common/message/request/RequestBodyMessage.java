package com.onlyxiahui.common.message.request;

/**
 * 信息<br>
 * Date 2018-12-25 09:41:05<br>
 * Description 用于发送请求时的消息体，body需要传进来<br>
 * Body body = new Body();<br>
 * body.setName("body");<br>
 * 
 * RequestBodyMessage rm = new RequestBodyMessage();<br>
 * rm.setBody(body);
 * 
 * Date 2020-01-14 20:48:17<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @param <B> 主体
 * @since 1.0.0
 */
public class RequestBodyMessage<B> extends AbstractRequestMessage<B> {

	public RequestBodyMessage() {
	}

	public RequestBodyMessage(B body) {
		setBody(body);
	}
}
