package com.onlyxiahui.common.message.request;

import java.util.HashMap;
import java.util.Map;

/**
 * 信息<br>
 * Date 2018-12-25 09:45:48<br>
 * Description 用于发送请求消息，已经含有body，只需添加body中数据就可以<br>
 * 
 * @author XiaHui<br>
 * @since 1.0.0
 */
public class RequestMessage extends AbstractRequestMessage<Map<String, Object>> {

	/**
	 * 主体
	 */
	private Map<String, Object> body = new HashMap<>();

	public RequestMessage() {
		setBody(body);
	}

	public Object bodyPut(String key, Object value) {
		return body.put(key, value);
	}

	public Object bodyGet(String key) {
		return body.get(key);
	}
}
