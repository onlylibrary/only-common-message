package com.onlyxiahui.common.message.request;

import java.util.HashMap;

import com.onlyxiahui.common.message.AbstractMessage;

/**
 * 信息<br>
 * Date 2018-12-25 10:58:04<br>
 * 
 * @author XiaHui<br>
 * @param <B> 主体
 * @since 1.0.0
 */
public abstract class AbstractRequestMessage<B> extends AbstractMessage<B> {

	@SuppressWarnings("unchecked")
	public AbstractRequestMessage() {
		setBody((B) new HashMap<>(0));
	}
}
