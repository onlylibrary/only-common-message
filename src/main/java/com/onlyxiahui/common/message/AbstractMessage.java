package com.onlyxiahui.common.message;

import com.onlyxiahui.common.message.node.Head;

/**
 * 信息<br>
 * Date 2018-12-25 10:58:37<br>
 * 
 * @author XiaHui<br>
 * @param <B> 主体
 * @since 1.0.0
 */
public abstract class AbstractMessage<B> implements Message<Head, B> {

	/**
	 * 消息头部
	 */
	protected Head head;
	/**
	 * 信息主体
	 */
	protected B body;

	public B getBody() {
		return body;
	}

	public void setBody(B body) {
		this.body = body;
	}

	@Override
	public Head getHead() {
		return this.head;
	}

	@Override
	public void setHead(Head head) {
		this.head = head;
	}
}
