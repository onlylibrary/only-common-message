package com.onlyxiahui.common.message;

import com.onlyxiahui.common.message.node.Head;

/**
 * 信息<br>
 * Date 2019-01-06 10:16:14<br>
 * 
 * @author XiaHui<br>
 * @param <H> 头部
 * @param <B> 主体
 * @since 1.0.0
 */
public interface Message<H extends Head, B> {

	/**
	 * 设置信息头部<br>
	 * Date 2019-01-06 10:41:27<br>
	 * Description 设置信息头部
	 * 
	 * @param head 头部
	 * @since 1.0.0
	 */
	public void setHead(H head);

	/**
	 * 获取信息头部<br>
	 * Date 2019-01-06 10:41:47<br>
	 * Description 获取信息头部
	 * 
	 * @return H 头部
	 * @since 1.0.0
	 */
	public H getHead();
}
