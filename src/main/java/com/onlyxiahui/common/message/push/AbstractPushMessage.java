package com.onlyxiahui.common.message.push;

import java.util.HashMap;

import com.onlyxiahui.common.message.AbstractMessage;

/**
 * 服务器推送信息 <br>
 * Date 2020-01-14 20:54:49<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @param <B> 主体
 * @since 1.0.0
 */
public abstract class AbstractPushMessage<B> extends AbstractMessage<B> {

	@SuppressWarnings("unchecked")
	public AbstractPushMessage() {
		setBody((B) new HashMap<>(0));
	}
}
