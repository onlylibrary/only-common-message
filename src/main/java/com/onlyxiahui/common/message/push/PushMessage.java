package com.onlyxiahui.common.message.push;

import java.util.HashMap;
import java.util.Map;

/**
 * 服务器推送信息<br>
 * Date 2018-12-25 11:09:32<br>
 * 
 * @author XiaHui<br>
 * @since 1.0.0
 */
public class PushMessage extends AbstractPushMessage<Map<String, Object>> {

	/**
	 * 信息主体
	 */
	private Map<String, Object> body = new HashMap<>();

	public PushMessage() {
		this.setBody(body);
	}

	public PushMessage(String key, Object data) {
		body.put(key, data);
	}

	@Override
	public Map<String, Object> getBody() {
		return body;
	}

	public Object bodyPut(String key, Object value) {
		return body.put(key, value);
	}

	public Object bodyGet(String key) {
		return body.get(key);
	}
}
