package com.onlyxiahui.common.message.push;

import java.util.HashMap;

/**
 * 服务器推送消息<br>
 * 其中body自定义对象<br>
 * Date 2018-12-25 11:08:54<br>
 * 
 * @author XiaHui<br>
 * @since 1.0.0
 */
public class PushBodyMessage<B> extends AbstractPushMessage<B> {

	@SuppressWarnings("unchecked")
	public PushBodyMessage() {
		setBody((B) new HashMap<>(0));
	}

	public PushBodyMessage(B body) {
		setBody(body);
	}
}
