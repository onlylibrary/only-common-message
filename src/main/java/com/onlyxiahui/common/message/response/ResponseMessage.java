package com.onlyxiahui.common.message.response;

import com.onlyxiahui.common.message.bean.Info;

/**
 * 信息<br>
 * Date 2018-12-25 10:56:45<br>
 * 
 * @author XiaHui<br>
 * @param <H> 头部
 * @param <B> 主体
 * @since 1.0.0
 */
public class ResponseMessage<H, B> {

	/**
	 * 消息状态信息（成功/失败）
	 */
	private Info info;
	/**
	 * 消息头部
	 */
	private H head;
	/**
	 * 消息主体
	 */
	private B body;

	public H getHead() {
		return head;
	}

	public void setHead(H head) {
		this.head = head;
	}

	public B getBody() {
		return body;
	}

	public void setBody(B body) {
		this.body = body;
	}

	public Info getInfo() {
		return info;
	}

	public void setInfo(Info info) {
		this.info = info;
	}

	public boolean isSuccess() {
		return null == info ? false : info.isSuccess();
	}
}
