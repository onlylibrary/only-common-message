package com.onlyxiahui.common.message;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import com.alibaba.fastjson.JSONObject;
import com.onlyxiahui.common.message.node.Head;
import com.onlyxiahui.common.message.push.PushMessage;
import com.onlyxiahui.common.message.request.RequestMessage;
import com.onlyxiahui.common.message.response.ResponseMessage;
import com.onlyxiahui.common.message.result.ResultBodyMessage;
import com.onlyxiahui.common.message.result.ResultMessage;

/**
 * 
 * Date 2018-12-25 11:11:42<br>
 * Description
 * 
 * @author XiaHui<br>
 * @since 1.0.0
 */
public class MessageTest {

	@Test
	public void showResultMessage() {
		Head head = getHead("001", "2.22");
		ResultMessage m = get(ResultMessage.class);
		m.setHead(head);
		m.bodyPut("name", "123");
		m.bodyPut("array", new String[] { "1", "2", "3", "4", "5" });
		m.addError("001", "错误");
		out(m);
	}

	@Test
	public void showResultMessageBody() {
		Head head = getHead("001", "2.22");
		Map<String, Object> body = getBody();
		ResultMessage m = get(ResultMessage.class);
		m.setHead(head);
		m.setBody(body);
		out(m);
		m.bodyPut("name", "123");
		out(m);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void showResultBodyMessage() {
		Head head = getHead("001", "2.22");
		Map<String, Object> body = getBody();
		ResultBodyMessage<Map<String, Object>> m = get(ResultBodyMessage.class);
		m.setHead(head);
		m.setBody(body);
		out(m);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void showResponseMessage() {
		// Head head = getHead("001", "2.22");
		// Map<String, Object> body = getBody();
		ResponseMessage<Head, HashMap<String, Object>> m = get(ResponseMessage.class);
		System.out.println(m);
	}

	@Test
	public void showRequestMessage() {
		Head head = getHead("001", "2.22");
		Map<String, Object> body = getBody();
		RequestMessage m = get(RequestMessage.class);
		m.setHead(head);
		m.bodyPut("name", "123");
		out(m);
		m.setBody(body);
		out(m);
		m.bodyPut("sex", "1");
		out(m);
	}

	@Test
	public void showPushMessage() {
		Head head = getHead("001", "2.22");
		Map<String, Object> body = getBody();
		PushMessage m = get(PushMessage.class);
		m.setHead(head);
		m.bodyPut("name", "123");
		out(m);
		m.setBody(body);
		out(m);
		m.bodyPut("sex", "1");
		out(m);
	}

	@Test
	public void showPushBodyMessage() {
		Head head = getHead("001", "2.22");
		Map<String, Object> body = getBody();
		PushMessage m = get(PushMessage.class);
		m.setHead(head);
		m.setBody(body);
		out(m);
	}

	@SuppressWarnings("unchecked")
	public <T> T get(Class<T> clazz) {
		Object object = null;
		try {
			object = clazz.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return (T) object;
	}

	public Map<String, Object> getBody() {
		Map<String, Object> map = new HashMap<String, Object>(1);
		map.put("data", "123456");
		return map;
	}

	public Head getHead(String a, String m) {
		HeadImpl h = new HeadImpl();
		h.setAction(a);
		h.setMethod(m);
		h.setTime(System.currentTimeMillis());
		return h;
	}

	public void out(Object m) {
		String json = JSONObject.toJSONString(m);
		System.out.println(m.getClass().getSimpleName() + ":" + json);
	}
}
