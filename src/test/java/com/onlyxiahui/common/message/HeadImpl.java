package com.onlyxiahui.common.message;

import com.onlyxiahui.common.message.node.Head;

/**
 * Date 2018-12-25 11:11:34<br>
 * Description
 * 
 * @author XiaHui<br>
 * @since 1.0.0
 */

public class HeadImpl implements Head {

	private String key;
	private String name;
	private String action;
	private String method;
	private String version;
	private long time;

	public static final String CODE_FAIL = "0";
	public static final String CODE_SUCCESS = "1";

	public HeadImpl() {
		name = "";
		version = "1";
	}

	@Override
	public String getKey() {
		return key;
	}

	@Override
	public void setKey(String key) {
		this.key = key;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public long getTime() {
		return time;
	}

	public void setTime(long time) {
		this.time = time;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}
}
